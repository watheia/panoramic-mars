# Check out https://hub.docker.com/_/node to select a new base image
FROM ruby:2.7.0 as builder

# ARG cnb_uid=1000
# ARG cnb_gid=1001

# Install packages that we want to make available at both build and run time
# RUN apk add --update --no-cache bash ca-certificates

# Create user and group
# RUN addgroup -g ${cnb_gid} cnb && \
# adduser -u ${cnb_uid} -G cnb -s /bin/bash -D cnb

# Set required CNB information
# ENV CNB_USER_ID=${cnb_uid}
# ENV CNB_GROUP_ID=${cnb_gid}

# Set required CNB information
# ARG stack_id="org.watheia.stacks.jekyll"
# ENV CNB_STACK_ID=${stack_id}
# LABEL io.buildpacks.stack.id=${stack_id}

# Create app directory (with user `node`)
# RUN mkdir -p /app && \
# chown -R ${CNB_USER_ID}:${CNB_GROUP_ID} /app

# Set user and group (as declared in base image)
# USER ${CNB_USER_ID}:${CNB_GROUP_ID}
# ADD . .
# Create app directory (with user `node`)

WORKDIR /app
ADD . .
RUN  gem install bundler && \
  bundle install && \
  bundle exec jekyll build -d public


# Serve build result in own image
FROM nginx:alpine
ENV HOST=0.0.0.0 PORT=5000
COPY --from=builder /app/public /usr/share/nginx/html